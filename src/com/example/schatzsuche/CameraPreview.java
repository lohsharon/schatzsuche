package com.example.schatzsuche;

import java.io.IOException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

@SuppressLint("ViewConstructor")
public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback{

    private SurfaceHolder mHolder;
    private Camera mCamera;
    private String TAG = "CamCamPreview";
	
    /**
     * Camera Preview Constructor
     * @param context: current context used in the preview
     * @param camera: camera used for showing the preview
     */
	public CameraPreview(Context context, Camera camera){
		super(context);
		mCamera = camera;
		
		// Install a SurfaceHolder.Callback 
		// to get notified when the underlying surface is created and destroyed.
		mHolder = getHolder();
		mHolder.addCallback(this);
	}
    
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.
		
		// if preview surface does not exist
        if (mHolder.getSurface() == null){
            return; 
            }
		
        // first, stop the preview before applying any changes
        try {
        	mCamera.stopPreview();
        } catch (Exception e){
        	// do nothing, tried to stop a non-existent preview
        }
        
        // APPLYING CHANGES
        // set preview size and make any resize, rotate or reformatting changes here
        Camera.Parameters parameters = mCamera.getParameters();	
        parameters.setRotation(90);
        parameters.setPictureSize(640,480);
        mCamera.setParameters(parameters);
        
        mCamera.setDisplayOrientation(90);
        
        // finally, re-start the preview with new setting
        try {
        	mCamera.setPreviewDisplay(mHolder);
        	mCamera.startPreview();
        } catch (Exception e){
        	Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
	}

	@Override
	public void surfaceCreated(SurfaceHolder arg0) {
		// The Surface has been created, now tell the camera where to draw the preview.
		try {
			mCamera.setPreviewDisplay(mHolder);
			mCamera.startPreview();
		} catch (IOException e){
			Log.d(TAG,"Error in setting camera preview: " + e.getMessage());
		}
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		// empty. Take care of releasing the Camera preview in your activity.
	}

}
