package com.example.schatzsuche;

import java.io.File;
import java.util.List;

import android.hardware.Camera;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class CamCam extends Activity {

	private String TAG = "CamCamActivity";
	
    private Camera mCamera;				// Camera Hardware
    private CameraPreview mPreview;		// View for CameraPreview
    private FrameLayout cameraPreview;	// Layout container for preview
    private LocationManager mLocManager;// Location Manager used in this application
	private Location mLocation;			// Location of the device
	
	private Bundle extra;
	private WebService ws = new WebService();
	public static String fileURI = "";
	private String chest_id; 
	private String bssid;
	private String wifi;
	public static String wsNotif = "null";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cam_cam);
		
		extra = getIntent().getExtras();
		chest_id = (String) extra.get("chest_id");
		bssid = (String) extra.get("bssid");
		System.out.println("Chest ID : " + chest_id);
		System.out.println("BSSID : " + bssid);
		
		if (checkCameraHardware(this)){
			mCamera = getCameraInstance();
			mPreview = new CameraPreview(this, mCamera);
			cameraPreview = (FrameLayout) findViewById(R.id.camera_preview);
			cameraPreview.addView(mPreview);
			
			// Initiating Geotagging related attribute
			mLocManager = (LocationManager) getSystemService(LOCATION_SERVICE);
			
			Log.d("TAG", "status:" + mLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER));
			
			if (mLocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
				Criteria criteria = new Criteria();
				String provider = mLocManager.getBestProvider(criteria, false);
				//mLocation = mLocManager.getLastKnownLocation(provider);
				mLocation = mLocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
				TextView text = (TextView) findViewById(R.id.text_message);
				text.setText("lati = "+ mLocation.getLatitude() +" ,long = "+mLocation.getLongitude());
				
			} else {
				Log.d(TAG,"provider is not enabled");
			}
			
			// Adding capture button management and listener
			final Button captureButton = (Button) findViewById(R.id.button_capture);
			captureButton.setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View arg0) {
						// Get an image from the camera
						captureButton.setEnabled(true);
						mCamera.takePicture(null, null, new PhotoHandler(getApplicationContext(),mLocation));
						
						System.out.println("file URI CamCam: " + fileURI);
						
						if (!"".equals(fileURI)) {
							TextView txtNotif = (TextView) findViewById(R.id.text_notif);
							txtNotif.setText("Sending : " + fileURI + "  ... Please Wait");
							File imgFile = new  File(fileURI);
							try {
								wifi = cekWifi(bssid);
								ws.acquireChest(chest_id, imgFile, bssid, wifi);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							finally {
								txtNotif.setText("Status : " + wsNotif);
								System.out.println("wsNotif : " + wsNotif);
							}
						}
						
						/*
						mCamera.autoFocus(new AutoFocusCallback(){
							@Override
							public void onAutoFocus(boolean arg0, Camera arg1) {
								captureButton.setEnabled(true);	
							}
						}); */
					}
				}
			);	
			
		} else {
			TextView text = (TextView) findViewById(R.id.text_message);
			text.setText("Sorry, Your device doesn't have appropriate camera");
		}
	}


	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cam_cam, menu);
		return true;
	}
	
	/* CAMERA RELATED FUNCTION AND PROCEDURE */
	
	/**
	 * A Function to check whether device has camera hardware or not
	 * @param context Context where this function applied
	 * @return boolean : True if Camera Hardware found, False if Camera Hardware not found
	 */
	private boolean checkCameraHardware(Context context) {
	    if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
	        return true;	// this device has a camera
	    } else {
	        return false; 	// no camera on this device
	    }
	}
	
	/**
	 * A safe procedure to get an instance of the Camera object.
	 * @return Camera: the obtained camera instance
	 */
	public static Camera getCameraInstance(){
	    Camera c = null;
	    try {
	        c = Camera.open(); // Attempt to get a Camera instance
	    }
	    catch (Exception e){
	        // Camera is not available (in use or does not exist)
	    }
	    return c;  // Returns null if camera is unavailable
	}
	
    /**
     * Procedure to release the camera resource
     */
    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();   // Release the camera for other applications     
            mCamera = null;
        }
    }
	
    /* ACTIVITY HANDLER */
    
    @Override
    protected void onPause() {
        super.onPause();
        mCamera.stopPreview();
        releaseCamera();	// Release the camera immediately on pause event

    }
    
    @Override
    protected void onResume(){
    	super.onResume();
    	
    	// Camera onResume Handling
    	if (mCamera == null){
    		mCamera = getCameraInstance(); // Re-open Camera immediately on resume event
        	mCamera.startPreview();
        	
    		// refreshing cameraPreview ()
    		if(cameraPreview != null){
    			cameraPreview.removeAllViews();
            	mPreview = new CameraPreview(this, mCamera);
            	//cameraPreview = (FrameLayout) findViewById(R.id.camera_preview);
            	cameraPreview.addView(mPreview);
    		}	
    	}
    }
    	
    @Override	
    public void onBackPressed(){
    	//mCamera.stopPreview();
        //releaseCamera();
		Intent backtoMain = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(backtoMain);
    }
    
	public String cekWifi(String bssid_) {
		WifiManager wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		List<ScanResult> scanResult = wifiManager.getScanResults();
		if (scanResult != null){
			for (int i = 0; i<scanResult.size(); i++){
				Log.e("SCANRESULT","Speed of wifi = "+scanResult.get(i).level);
				Log.e("BSSI",scanResult.get(i).BSSID);
				if (bssid_.equals(scanResult.get(i).BSSID)) {
					System.out.println("SAMA!");
					return ""+scanResult.get(i).level;
				}
			}
		}
		return "0";
	}
    
}
