package com.example.schatzsuche;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.location.Location;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Log;

@SuppressLint({ "SimpleDateFormat", "UseValueOf" })
public class PhotoHandler implements PictureCallback{
	
	private static final int MEDIA_TYPE_IMAGE = 1;
	private static final int MEDIA_TYPE_VIDEO = 2;
	private static final String TAG = "CamPhotoHandler";
	
	private final Context context;
	private static String fileUri = "";
	private Location mLocation;
	
	/* CONSTRUCTOR */
	
	/**
	 * PhotoHandler Class Constructor
	 * @param context : current context used in photohandler
	 */
	public PhotoHandler(Context context, Location location){
		this.context = context;
		this.mLocation = location;
	}
	
    /* GETTER AND SETTER */
  
    /* METHOD */
    
	 @Override
	public void onPictureTaken(byte[] data, Camera camera) {
		// Handle every action when the picture is taken by camera
		
		// Creating a Container File 
		File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
        if (pictureFile == null){
            Log.d(TAG, "Error creating media file, check storage permissions: ");
            return;
        }

        // Writing data to the provided File
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            fos.write(data);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
        }
        
        Log.d(TAG,"lon: "+ mLocation.getLongitude());
        loc2Exif(fileUri, mLocation);
        System.out.println("file URI : " + fileUri);
        CamCam.fileURI = fileUri;
        fileUri = "";
        camera.startPreview();
	}
	
	/**
	 * Function to create a file for saving an image or video
	 * @param type : Type of file which will be created (1: Picture, 2: Video)
	 * @return File: file needed for saving the image or video
	 */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "YukiCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg");
            fileUri = mediaStorageDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg";
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator + "VID_"+ timeStamp + ".mp4");
            fileUri = mediaStorageDir.getPath() + File.separator + "VID_"+ timeStamp + ".mp4";
        } else {
            return null;
        }
        
        return mediaFile;
    }

    /* GEOTAGGING FUNCTION */
    
    /**
     * Procedure used to transform and inserting location to jpeg picture using exif interface
     * @param flNm : path directory + name + extension of file which will be edited
     * @param loc : location data which will be inserted
     */
    public void loc2Exif(String flNm, Location loc) {
    	try {
    		ExifInterface ef = new ExifInterface(flNm);
    		ef.setAttribute(ExifInterface.TAG_GPS_LATITUDE, dec2DMS(loc.getLatitude()));
    		ef.setAttribute(ExifInterface.TAG_GPS_LONGITUDE,dec2DMS(loc.getLongitude()));
    		
    		if (loc.getLatitude() > 0){
    			ef.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "N");
    		} else {
    			ef.setAttribute(ExifInterface.TAG_GPS_LATITUDE_REF, "S");
    		}

    		if (loc.getLongitude()>0){
    			ef.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "E");
    		} else {
    			ef.setAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF, "W");
    		}
    		
    		ef.saveAttributes();
    	} catch (IOException e) {}         
    }
    
	public String dec2DMS(double coord) {  
		coord = coord > 0 ? coord : -coord;  // -105.9876543 -> 105.9876543
		String sOut = Integer.toString((int)coord) + "/1,";   // 105/1,
		coord = (coord % 1) * 60;         // .987654321 * 60 = 59.259258
		sOut = sOut + Integer.toString((int)coord) + "/1,";   // 105/1,59/1,
		coord = (coord % 1) * 60000;             // .259258 * 60000 = 15555
		sOut = sOut + Integer.toString((int)coord) + "/1000";   // 105/1,59/1,15555/1000
		return sOut;
	}
    
	/**
	 * Function to obtain location data from jpeg picture using exif interface
	 * @param flNm : directory path + name + extension of the file which information will be extracted
	 * @return loc : Location data from jpeg file
	 */
	public Location exif2Loc(String flNm) {
		String sLat = "", sLatR = "", sLon = "", sLonR = "";
		
		try {
			ExifInterface ef = new ExifInterface(flNm);
			sLat  = ef.getAttribute(ExifInterface.TAG_GPS_LATITUDE);
			sLon  = ef.getAttribute(ExifInterface.TAG_GPS_LONGITUDE);
			sLatR = ef.getAttribute(ExifInterface.TAG_GPS_LATITUDE_REF);
			sLonR = ef.getAttribute(ExifInterface.TAG_GPS_LONGITUDE_REF);
		} catch (IOException e) {
			return null;
		}

		double lat = dms2Dbl(sLat);
		if (lat > 180.0) {return null;}
		
		double lon = dms2Dbl(sLon);
		if (lon > 180.0) {return null;}

		lat = sLatR.contains("S") ? -lat : lat;
  		lon = sLonR.contains("W") ? -lon : lon;

  		Location loc = new Location("exif");
  		loc.setLatitude(lat);
  		loc.setLongitude(lon);
  		return loc;
	}
    	
	public double dms2Dbl(String sDMS){
		double dRV = 999.0;
		
		try {
			String[] DMSs = sDMS.split(",", 3);
			String s[] = DMSs[0].split("/", 2);
			dRV = (new Double(s[0])/new Double(s[1]));
			s = DMSs[1].split("/", 2);
			dRV += ((new Double(s[0])/new Double(s[1]))/60);
			s = DMSs[2].split("/", 2);
			dRV += ((new Double(s[0])/new Double(s[1]))/3600);
		} catch (Exception e) {}
		return dRV;
	}
	
}
